
MaesboxCommonBundle
===

[![Latest Stable Version](https://poser.pugx.org/maesbox/common-bundle/v/stable)](https://packagist.org/packages/maesbox/common-bundle)
[![Total Downloads](https://poser.pugx.org/maesbox/common-bundle/downloads)](https://packagist.org/packages/maesbox/common-bundle)
[![License](https://poser.pugx.org/maesbox/common-bundle/license)](https://packagist.org/packages/maesbox/common-bundle)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/d30fc805-61d9-4a04-ae2f-5a472e93a57a/mini.png)](https://insight.sensiolabs.com/projects/d30fc805-61d9-4a04-ae2f-5a472e93a57a)

installation
---

### composer

`composer require maesbox/commonbundle`

### kernel

add these lines to `app/AppKernel.php` 

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
		//...
			new Sonata\SeoBundle\SonataSeoBundle(),
			new Symfony\Cmf\Bundle\SeoBundle\CmfSeoBundle(),
			new JMS\SerializerBundle\JMSSerializerBundle(),
			new JMS\AopBundle\JMSAopBundle(),
			new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
			new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
			new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
		//...
			new Maesbox\CommonBundle\MaesboxCommonBundle(),
		//...
		);
	}
	//...
}
```

configuration
---

### MaesboxCommonBundle

```yml
doctrine:
    orm:
        entity_managers:   
            default:
                dql:
                    datetime_functions:
                        #date: YourApp\YourBundle\DQL\Date
                        #hour: YourApp\YourBundle\DQL\Hour
                        month: Maesbox\CommonBundle\Model\DQL\Month
                        year: Maesbox\CommonBundle\Model\DQL\Year
```

### StofDoctrineExtensionsBundle

```yml
doctrine:
    orm:
        entity_managers:
            default:
                mappings:
                    gedmo_translatable:
                        type: annotation
                        prefix: Gedmo\Translatable\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"
                        alias: GedmoTranslatable
                        is_bundle: false
                    gedmo_translator:
                        type: annotation
                        prefix: Gedmo\Translator\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translator/Entity"
                        alias: GedmoTranslator 
                        is_bundle: false
                    gedmo_loggable:
                        type: annotation
                        prefix: Gedmo\Loggable\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity"
                        alias: GedmoLoggable 
                        is_bundle: false
                    gedmo_tree:
                        type: annotation
                        prefix: Gedmo\Tree\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Tree/Entity"
                        alias: GedmoTree 
                        is_bundle: false
```

for more advanced options, refer to bundle docs:

 * [JMSSecurityExtraBundle][1]
 * [JMSSerializerBundle][2]
 * [KnpPaginatorBundle][3]
 * [StofDoctrinesExtensionsBundle][4]



features
---

### controller

```php
<?php

use Maesbox\CommonBundle\Model\BaseClass\BaseController;

class AppController extends BaseController
{

}

```

provides several methods:

* getSession
* getSerializer
* getParameter
* getEventDispatcher
* getManager
* getBaseManager
* getRepository
* getSecurity
* getCurrentUser
* getTranslator

### entity manager

* firstly your manager must extend `Maesbox\CommonBundle\Model\BaseClass\BaseManager`

```php
<?php

use Maesbox\CommonBundle\Model\BaseClass\BaseManager;

class AppManager extends BaseManager
{

}

```

* secondly you have to annotate the reposiotry methods you want to expose

```php
<?php

use Doctrine\ORM\EntityRepository;

use Maesbox\CommonBundle\Model\Annotation\SingleResultQuery;
use Maesbox\CommonBundle\Model\Annotation\SingleScalarResultQuery;
use Maesbox\CommonBundle\Model\Annotation\ListResultQuery;
use Maesbox\CommonBundle\Model\Annotation\ListScalarResultQuery;

class AppRepository extends EntityRepository
{
	/**
	 * @SingleResultQuery
	 */
	public function getSingleResultQuery($params){
		return $this->createQueryBuilder("a");
	}

	/**
	 * @SingleScalarResultQuery
	 */
	public function getSingleScalarResultQuery($params){
		return $this->createQueryBuilder("a");
	}

	/**
	 * @ListResultQuery
	 */
	public function getListResultQuery($params){
		return $this->createQueryBuilder("a");
	}

	/**
	 * @ScalarListResultQuery
	 */
	public function getScalarListResultQuery($params){
		return $this->createQueryBuilder("a");
	}
}
```


### listener


* console listener

	logs console errors :)

####




[1]: http://jmsyst.com/bundles/JMSSecurityExtraBundle
[2]: http://jmsyst.com/bundles/JMSSerializerBundle
[3]: https://github.com/KnpLabs/KnpPaginatorBundle
[4]: http://symfony.com/doc/current/bundles/StofDoctrineExtensionsBundle/index.html

