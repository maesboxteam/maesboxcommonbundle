<?php

// tests/AppBundle/Controller/PostControllerTest.php
namespace Maesbox\CommonBundle\Tests\BaseClass;

use Maesbox\CommonBundle\Tests\Manager\TestManager;

use Maesbox\CommonBundle\Tests\Repository\TestRepository;

use Doctrine\Common\Persistence\ObjectManager;

class BaseManagerTest extends \PHPUnit_Framework_TestCase
{
    public $test_manager;

    public $test_repository;

    public $test_paginator;
	
	public $entity_manager;

    public function setUp()
    {
        parent::setUp();
		
        $this->entity_manager = $this
            ->getMockBuilder(ObjectManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->test_repository = $this
            ->getMockBuilder(TestRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        
		$this->entity_manager->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->test_repository);

        $this->test_manager = new TestManager($this->entity_manager, "MaesboxCommonBundle:Test", $this->test_paginator);
	}	

    public function testBase()
    {	
		$this->assertEquals($this->test_repository, $this->test_manager->getRepository(), "testing repository");
		$this->assertEquals($this->entity_manager, $this->test_manager->getEntityManager(), "testing entity manager");
    }
}
