<?php

namespace Maesbox\CommonBundle\Tests\BaseClass;

use Maesbox\CommonBundle\Tests\Controller\TestController;

class BaseControllerTest extends \PHPUnit_Framework_TestCase
{
	public $test_controller;
	
	public function setUp()
    {
        parent::setUp();
		
		$this->test_controller = new TestController();
	}
}
