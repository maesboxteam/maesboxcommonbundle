<?php

namespace Maesbox\CommonBundle\Tests\Repository;

use Doctrine\ORM\EntityRepository;
use Maesbox\CommonBundle\Annotation\SingleResultQuery;
use Maesbox\CommonBundle\Annotation\SingleScalarResultQuery;

class TestRepository extends EntityRepository
{
    /**
     * @SingleResultQuery
     */
    public function getSingleResult()
    {
        return $this->createQueryBuilder('t');
    }

    /**
     * @SingleScalarResultQuery
     */
    public function getSinglescalarResult()
    {
        return $this->createQueryBuilder('t');
    }
}
