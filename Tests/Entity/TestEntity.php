<?php

namespace Maesbox\CommonBundle\Tests\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestEntity.
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\CommonlBundle\Tests\Utils\TestRepository")
 */
class TestEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
}
