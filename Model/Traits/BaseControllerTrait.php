<?php

namespace Maesbox\CommonBundle\Model\Traits;

use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait BaseControllerTrait
{
    public function getParameter($parameter)
    {
        return $this->container->getParameter($parameter);
    }

    /**
     * @return EventDispatcher
     *
     * @throws \LogicException
     */
    public function getEventDispatcher()
    {
        if (!$this->container->has('event_dispatcher')) {
            throw new \LogicException('The event_dispatcher is not registered in your application.');
        }

        return $this->container->get('event_dispatcher');
    }

    /**
     * @return Session
     *
     * @throws \LogicException
     */
    public function getSession()
    {
        if (!$this->container->has('session')) {
            throw new \LogicException('The session is not registered in your application.');
        }

        return $this->container->get('session');
    }

    /**
     * @return TokenStorageInterface
     *
     * @throws \LogicException
     */
    public function getSecurity()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The security.token_storage is not registered in your application.');
        }

        return $this->container->get('security.token_storage');
    }

    /**
     * @return null|User
     */
    public function getCurrentUser()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = null;
        } else {
			$user = $this->getSecurity()->getToken()->getUser();
		}

        return $user;
    }

    /**
     * @return Serializer
     *
     * @throws \LogicException
     */
    public function getSerializer()
    {
        if ($this->container->has('jms_serializer')) {
            $serializer = $this->container->get('jms_serializer');
        } elseif ($this->container->has('serializer')) {
            $serializer = $this->container->get('serializer');
        } else {
            throw new \LogicException('The serializer is not registered in your application.');
        }
		
		return $serializer;
    }

    public function getTranslator()
    {
        if (!$this->container->has('translator')) {
            throw new \LogicException('The translator is not registered in your application.');
        }

        return $this->container->get('translator');
    }
	
	/**
	 * @return \Psr\Log\LoggerInterface
	 * @throws \LogicException
	 */
	public function getLogger()
	{
		if (!$this->container->has('logger')) {
			throw new \LogicException('The logger is not registered in your application.');
		}
		return $this->container->get('logger');
	}
 

    public function getBaseManager($name = null)
    {
        return ($name === null) ? $this->container->get('doctrine')->getManager() : $this->container->get('doctrine')->getManager($name);
    }

    public function getManager($className)
    {
        return $this->get($className);
    }

    public function getRepository($className, $manager = null)
    {
        return  ($manager === null) ? $this->getBaseManager()->getRepository($className) : $this->getBaseManager($manager)->getRepository($className);
    }
}
