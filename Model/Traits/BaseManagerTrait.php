<?php

namespace Maesbox\CommonBundle\Model\Traits;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Annotations\AnnotationReader;

use Knp\Component\Pager\PaginatorInterface;

trait BaseManagerTrait
{
    /**
     * @var ObjectManager
     */
    protected $em;

    protected $repository;
	
	/**
	 * @var PaginatorInterface 
	 */
    protected $paginator;
	
    public function __call($name, $arguments)
    {
        $method = $this->getQueryMethod($name);

        if ($method) {
			if ($this->isValidQueryMethod($method, $arguments)) {
                return $this->executeMethod($this->getQueryMethod($name), $arguments);
            } else {
                throw new \Exception('Invalid arguments for method '.$name);
            }
        } else {
            throw new \Exception('Unknown method '.$name);
        }
    }
	
    public function __construct(ObjectManager $em, $repository, $paginator = null)
    {
        $this->setEntityManager($em)
			->setRepositoryByName($repository)
			->setPaginator($paginator);
    }

    public function clear()
    {
        $this->em->clear();
    }

    public function flush()
    {
        $this->em->flush();
    }

    protected function removeAndFlush($entity)
    {
        $this->em->remove($entity);
        $this->flush();
    }

    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->flush();
    }

    protected function mergeAndFlush($entity)
    {
        $this->em->merge($entity);
        $this->flush();
    }
	
	/**
	 * @param ObjectManager $manager
	 */
	public function setEntityManager(ObjectManager $manager)
	{
		$this->em = $manager;
		return $this;
	}
	
	/**
	 * @return ObjectManager
	 */
	public function getEntityManager()
	{
		return $this->em;
	}
	
	/**
	 * @param type $repository
	 */
	public function setRepositoryByName($repository)
	{
		return $this->setRepository($this->getEntityManager()->getRepository($repository));
	}
	
	/**
	 * @param type $repository
	 */
	public function setRepository($repository)
	{
		$this->repository = $repository;
		return $this;
	}
	
    public function getRepository()
    {
        return $this->repository;
    }
	
	/**
	 * @param PaginatorInterface $paginator
	 * @return $this
	 */
	public function setPaginator(PaginatorInterface $paginator)
	{
		$this->paginator = $paginator;
		return $this;
	}
	
	/**
	 * @return PaginatorInterface
	 */
    public function getPaginator()
    {
        return $this->paginator;
    }

    public function get($id)
    {
        return $this->getRepository()->find($id);
    }

    public function all()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param string $name
     *
     * @return \ReflectionMethod|null
     */
    public function getQueryMethod($name)
    {
        foreach ($this->getRepositoryQueryMethods() as $method) {
            if ($method->getName() === $name) {
                return $method;
            }
        }

        return null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function isSingleResultQuery(\ReflectionMethod $method)
    {
        $reader = new AnnotationReader();

        return $reader->getMethodAnnotation($method, 'Maesbox\\CommonBundle\\Model\\Annotation\\SingleResultQuery') !== null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function isSingleScalarResultQuery(\ReflectionMethod $method)
    {
        $reader = new AnnotationReader();

        return $reader->getMethodAnnotation($method, 'Maesbox\\CommonBundle\\Model\\Annotation\\SingleScalarResultQuery') !== null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function isListResultQuery(\ReflectionMethod $method)
    {
        $reader = new AnnotationReader();

        return $reader->getMethodAnnotation($method, 'Maesbox\\CommonBundle\\Model\\Annotation\\ListResultQuery') !== null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function isScalarListResultQuery(\ReflectionMethod $method)
    {
        $reader = new AnnotationReader();

        return $reader->getMethodAnnotation($method, 'Maesbox\\CommonBundle\\Model\\Annotation\\ScalarListResultQuery') !== null;
    }

    /**
     * @return array
     */
    public function getRepositoryQueryMethods()
    {
        $methods = [];
        $reflectionClass = new \ReflectionClass(get_class($this->getRepository()));

        foreach ($reflectionClass->getMethods() as $method) {
            if ($this->isSingleResultQuery($method) ||
                $this->isSingleScalarResultQuery($method) ||
                $this->isListResultQuery($method) ||
                $this->isScalarListResultQuery($method)
                ) {
                $methods[] = $method;
            }
        }

        return $methods;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function isAvailableQueryMethod(\ReflectionMethod $method)
    {
        return in_array($method, $this->getRepositoryQueryMethods());
    }

    /**
     * @param \ReflectionMethod $method
     * @param array             $arguments
     *
     * @return bool
     */
    private function areAvailableQueryMethodParameters(\ReflectionMethod $method, array $arguments)
    {
        if ($this->isListResultQuery($method)) {
            return count($method->getParameters()) === count($arguments) || $this->isValidPagedMethodCall($method, $arguments);
        } else {
            return count($method->getParameters()) === count($arguments);
        }
    }

    /**
     * @param \ReflectionMethod $method
     * @param array             $arguments
     *
     * @return bool
     */
    private function isValidQueryMethod(\ReflectionMethod $method, array $arguments)
    {
        return $this->isAvailableQueryMethod($method) && $this->areAvailableQueryMethodParameters($method, $arguments);
    }

    /**
     * @param array $arguments
     *
     * @return array
     */
    private function getPagedMethodParameters(array $arguments)
    {
        return array_slice($arguments, 2);
    }

    /**
     * @param array $arguments
     *
     * @return array
     */
    private function getPagedMethodPaginationParameters(array $arguments)
    {
        return array_slice($arguments, 0, 2);
    }

    /**
     * @param \ReflectionMethod $method
     * @param array             $arguments
     *
     * @return bool
     */
    private function isValidPagedMethodCall(\ReflectionMethod $method, array $arguments)
    {
        return $this->areValidMethodParameters($method, $this->getPagedMethodParameters($arguments));
    }

    /**
     * @param \ReflectionMethod $method
     * @param array             $arguments
     *
     * @return bool
     */
    private function areValidMethodParameters(\ReflectionMethod $method, array $arguments)
    {
        return count($method->getParameters()) === count($arguments);
    }

    /**
     * @param \ReflectionMethod $method
     * @param array             $arguments
     *
     * @return mixed
     */
    private function executeMethod(\ReflectionMethod $method, array $arguments)
    {
        if($this->isListResultQuery($method) && $this->isValidPagedMethodCall($method, $arguments) ) {
			$qb = call_user_func_array(array($this->getRepository(), $method->getName()), $this->getPagedMethodParameters($arguments));
			
			$pagination = $this->getPagedMethodPaginationParameters($arguments);

			$result = $this->getPaginator()->paginate(
				$qb->getQuery(),
				$pagination[0], //page number
				$pagination[1] // nb elems
			);
		} else {
			$qb = call_user_func_array(array($this->getRepository(), $method->getName()), $arguments);
			
			if($this->isSingleResultQuery($method)) {
				$result = $qb->getQuery()->getOneOrNullResult();
			} 
			elseif($this->isSingleScalarResultQuery($method)) {
				$result = $qb->getQuery()->getScalarResult();
			}
			elseif ($this->isScalarListResultQuery($method)) {
				$result = $qb->getQuery()->getScalarResult();
			}
			elseif ($this->isListResultQuery($method) && !$this->isValidPagedMethodCall($method, $arguments)) {
                $result = $qb->getQuery()->getResult();
            }
		}
		
		return $result;
    }
}
