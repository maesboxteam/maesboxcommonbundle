<?php

namespace Maesbox\CommonBundle\Model\BaseClass;

use Maesbox\CommonBundle\Model\Traits\BaseManagerTrait;

abstract class BaseManager
{
    use BaseManagerTrait;
}
