<?php

namespace Maesbox\CommonBundle\Model\BaseClass;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maesbox\CommonBundle\Model\Traits\BaseControllerTrait;

abstract class BaseController extends Controller
{
    use BaseControllerTrait;
}
