<?php

namespace Maesbox\CommonBundle\Model\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
final class SingleScalarResultQuery
{
    /**
     * Parameter name.
     *
     * @var string
     */
    public $name;
    /**
     * Parameter description.
     *
     * @var string
     */
    public $description;
}
