<?php

namespace Maesbox\CommonBundle\Model\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
final class SingleResultQuery
{
    /**
     * Parameter name.
     *
     * @var string
     */
    public $name;
    /**
     * Parameter description.
     *
     * @var string
     */
    public $description;
}
