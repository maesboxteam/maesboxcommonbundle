<?php

namespace Maesbox\CommonBundle\Model\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
final class ScalarListResultQuery
{
    /**
     * Parameter name.
     *
     * @var string
     */
    public $name;
    /**
     * Parameter description.
     *
     * @var string
     */
    public $description;
}
